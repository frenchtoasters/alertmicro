import pymysql.cursors
import datetime
import pytz

def log(data, *args):
    """
    Log any message in a uniform format
    """
    print(data)


connection = pymysql.connect(host='mysql-db', 
                             user='toast', 
                             password='password', 
                             db='alerts', charset='utf8mb4', 
                             cursorclass=pymysql.cursors.DictCursor)

try: 
    with connection.cursor() as cursor:
        sql = "INSERT INTO `alert` (alertId) VALUES (%s), (%s), (%s)"
        cursor.execute(sql, ('1234','5432', '1423'))

    connection.commit()

    with connection.cursor() as cursor:
        sql = "select * from alert"
        cursor.execute(sql)
        result = cursor.fetchall()
        log("Database<{0}>".format(result))
finally:
    connection.close()
