import pymysql.cursors
import datetime
import pytz
import yaml

def log(data, *args):
    """
    Log any message in a uniform format
    """
    print(data)


connection = pymysql.connect(host='mysql-db', 
                             user='toast', 
                             password='password', 
                             db='alerts', charset='utf8mb4', 
                             cursorclass=pymysql.cursors.DictCursor)

try: 
    with open('db.yml', 'r') as state:
        cur_state = yaml.safe_load(state)
    
    with connection.cursor() as cursor:
        sql = "select * from alert"
        cursor.execute(sql)
        results = cursor.fetchall()
        log("Database<{0}>".format(results))
        cur_state = dict()
        cur_state['alerts'] = []
        print(cur_state)
        for result in results:
            cur_state['alerts'].append(result)
        with open('db.yml', 'w') as outfile:
            yaml.safe_dump(cur_state, outfile)

finally:
    connection.close()
