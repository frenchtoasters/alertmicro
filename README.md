# Alert Micro

This is Mirco service design that does the following:

* Deploys a `mysql` service 
* Runs the `parser.py` script to insert values into the `alerts` database
* Runs the `controller.py` script to handle values that were inserted in `alerts` database

# Design

* Parser: XXXXX # Gathers Alert Data
    * Gathers active alerts from system: XXXXX
    * Writes end state file

* CreateUpdate: # Creates and updates alert BO 
    * Loads saved state
    * Gathers previous state from system: YYYY
    * Reconciles state
        * Compares previous with current
            * Update alert BO with correct information
            * Creates BO for new alert
        * Writes end state file

* Updates state file in repo? 
    * Commit state file to Git Repo
        * Works I guess?

* Push to object storage? 
    * Send JSON state file to object storage
        * File will be only active alerts
        * Could encrypt easily 

* Write to external DB?
    * Write JSON blob to external MySQL db
        * We are loading it into JSON format to parse anyway


## Design Decisions

* A Parser could be broken down into the following parts:

    * Auth to System: XXXX 
    * Load previous alerts
    * Query System: XXXX for active alerts
    * Write resulting state file
    * Logout of System: XXXX

You would choose to write a state file of a simple JSON object out of simplicity. The idea is to just store the state file in some short lived persistent storage format for it to later be consumed by the next step in the process. 

* That next step is the Create Update aspect of this service. Here you load the state from your source of truth, the BO's that are tied to your incidents:

    * Auth to System: YYYY
    * Load current alert BO's that are active
    * Load state file from Parser: XXXX
    * Combine results of alert BO's and state file from Parser: XXXX
    * Write state file for backup 
    * Update source of truth with latest results of new State

This is where you save your long term persistent data that you will need. The parsers data being passed here should be thought of as a cache and nothing more. You may choose to always keep a second copy of this for HA or redudency, that is where you have to ask what do you want to manage along with this? API/Encryption keys for a file, which encryption would slow down the Loading process. Or would you want to use some traditional system like MySQL? Both give the application an external dependency, in that they need to know how to handle responses from this "archival process". This problem is easily solved in Gitlab with automatic moving of job artifacts to Object storage. This process would remove the need for logic in the Create Update aspect to be handle what to do if it cant just simply save something to disk to be collected and stored as artifacts. 
