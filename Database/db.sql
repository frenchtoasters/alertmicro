CREATE DATABASE alerts;

USE alerts;

CREATE TABLE alert (alertId int(11));

CREATE USER 'toast'@'172.17.%.%' IDENTIFIED by 'USER_PASS';

GRANT ALL PRIVILEGES ON alerts.* TO 'toast'@'172.17.%.%';

FLUSH PRIVILEGES; 
